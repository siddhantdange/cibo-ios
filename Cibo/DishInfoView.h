//
//  DishInfoScreen.h
//  Cibo
//
//  Created by Siddhant Dange on 1/23/15.
//  Copyright (c) 2015 Siddhant Dange. All rights reserved.
//

@import UIKit;

@interface DishInfoView : UIView
@property (weak, nonatomic) IBOutlet UILabel *dishNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *restaurantNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *dishTopNameLabel;

-(void)setDishName:(NSString*)dishName;

@end
