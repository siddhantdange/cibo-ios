//
//  AppViewController.h
//  Test1
//
//  Created by Siddhant Dange on 5/10/15.
//  Copyright (c) 2015 Siddhant Dange. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppViewController : UIViewController

-(instancetype)initWithViewControllersInMenu:(NSArray*)vcArr forTitles:(NSDictionary*)vcTitles withInitialViewController:(Class)initVc;
-(void)loadViewController:(Class)vcClass;

-(void)showMenuButton;
-(void)hideMenuButton;

-(void)openMenu;
-(void)closeMenu;

+ (instancetype)sharedInstance;

@end
