//
//  CBLikedDishCollectionViewCell.h
//  Cibo
//
//  Created by Siddhant Dange on 5/29/15.
//  Copyright (c) 2015 Siddhant Dange. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CBLikedDishCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIScrollView *containerView;
@property (nonatomic, weak) IBOutlet UILabel *dishNameLabel;
@property (weak, nonatomic) IBOutlet UIView *fadeView;

@end
