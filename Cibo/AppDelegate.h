//
//  AppDelegate.h
//  Cibo
//
//  Created by Siddhant Dange on 10/20/14.
//  Copyright (c) 2014 Siddhant Dange. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ScreenNavigation;
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) ScreenNavigation *screenNavigation;


@end

