//
//  DishInfoScreen.m
//  Cibo
//
//  Created by Siddhant Dange on 1/23/15.
//  Copyright (c) 2015 Siddhant Dange. All rights reserved.
//

#import "DishInfoView.h"
#import "ScreenNavigation.h"
#import "AppDelegate.h"

@interface DishInfoView ()
@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UIVisualEffectView *effectView;

@end

@implementation DishInfoView

-(instancetype)init{
    self = [super init];
    if(self){
        self = (DishInfoView*)[[[NSBundle mainBundle] loadNibNamed:@"DishInfoView" owner:self options:nil] objectAtIndex:0];
        //[self addSubview:_mainView];
    }
    
    return self;
}

-(void)setDishName:(NSString *)dishName{
    [_dishNameLabel setText:dishName];
    [_dishTopNameLabel setText:dishName];
}


@end
