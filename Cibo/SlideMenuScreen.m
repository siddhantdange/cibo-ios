//
//  SlideMenuScreen.m
//  Pal
//
//  Created by Siddhant Dange on 7/22/14.
//  Copyright (c) 2014 Siddhant Dange. All rights reserved.
//

#import "SlideMenuScreen.h"
#import "AppDelegate.h"

@interface SlideMenuScreen ()

@property (nonatomic, weak) IBOutlet UITableView *menuTable;
@property (nonatomic, assign) BOOL isVertical;
@property (nonatomic, assign) float showValue;
@property (nonatomic, strong) NSArray *menuTitles;
@property (nonatomic, strong) NSDictionary *menuMappings;

@end

@implementation SlideMenuScreen

-(void)setup{
    
    _menuMappings = @{
                      @"About" : @"AboutScreen"
                      };
    _menuTitles = _menuMappings.allKeys;
    _menuTable.delegate = self;
    _menuTable.dataSource = self;
    _isVertical = YES;
    _showValue = _menuTable.frame.origin.y + _menuTable.frame.size.height;
}


-(id)initWithVerticalSetting:(BOOL)isVertical{
    self = [super init];
    
    if(self){
        
        _isVertical = isVertical;
    }
    
    return self;
}

-(void)show{
    AppDelegate *delegate = [UIApplication sharedApplication].delegate;
    
    [delegate.menuWindow makeKeyAndVisible];
    delegate.menuWindow.alpha = 0.0f;
    
    [UIView animateWithDuration:0.2f animations:^{
        //move main window down
        CGRect mainWindowRect = delegate.mainWindow.frame;
        mainWindowRect.origin.y += _showValue;
        delegate.mainWindow.frame = mainWindowRect;
        
        //fade in menu
        delegate.menuWindow.alpha = 1.0f;
    } completion:^(BOOL finished) {
    }];
    
}

-(IBAction)topAreaHit:(id)sender{
    [self close];
}

-(void)close{
    AppDelegate *delegate = [UIApplication sharedApplication].delegate;
    
    
    [delegate.mainWindow makeKeyAndVisible];
    [UIView animateWithDuration:0.2f animations:^{
        //fade out menu
        delegate.menuWindow.alpha = 0.0f;
        
        //move main screen up
        CGRect mainWindowRect = delegate.mainWindow.frame;
        mainWindowRect.origin.y -= _showValue;
        delegate.mainWindow.frame = mainWindowRect;
    } completion:^(BOOL finished) {
    }];
    
    
}

#pragma -mark UITableViewDataSource Methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    int numRows = (int)_menuTitles.count;
    return numRows;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    // Identifier for retrieving reusable cells.
    static NSString *cellIdentifier = @"cellIdentifier";
    
    // Attempt to request the reusable cell.
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    // No cell available - create one.
    if(cell == nil) {
        cell = [[UITableViewCell alloc] init];
    }
    
    [cell.textLabel setText:_menuTitles[indexPath.row]];
    
    return cell;
}

#pragma -mark UITableViewDelegate Methods

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *screenName = _menuMappings[_menuTitles[indexPath.row]];
    [self goToScreen:screenName animated:YES];
}

@end
