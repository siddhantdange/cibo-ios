//
//  APIConnector.h
//  Cibo
//
//  Created by Siddhant Dange on 10/22/14.
//  Copyright (c) 2014 Siddhant Dange. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface APIConnector : NSObject

-(void)createUser:(NSDictionary*)data completion:(void(^)(NSDictionary*))completion;
-(void)loginUser:(NSDictionary*)data completion:(void(^)(NSDictionary*))completion;

-(void)loadNewFoodWithCompletion:(void(^)(NSDictionary*))completion;
-(void)sendBehavior:(NSString*)behaviorType forFoodId:(NSString*)foodId completion:(void(^)(NSDictionary*))completion;

@end
