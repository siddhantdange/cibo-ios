//
//  ViewController.m
//  Cibo
//
//  Created by Siddhant Dange on 10/20/14.
//  Copyright (c) 2014 Siddhant Dange. All rights reserved.
//

#import "CBLoginViewController.h"
#import "APIConnector.h"
#import "Installation.h"
#import "CBDishDiscoveyViewController.h"
#import "AppViewController.h"
#import "CBDish.h"
#import "User.h"


@interface CBLoginViewController ()
@property (weak, nonatomic) IBOutlet FBLoginView *fbLoginButton;
@property (nonatomic, strong) id<FBGraphUser>user;
@property (nonatomic, strong) NSString *fbAccessToken;

@end

@implementation CBLoginViewController

-(instancetype)init{
    self = [super init];
    if(self){
        [[NSBundle mainBundle] loadNibNamed:@"CBLoginViewController" owner:self options:nil];
        _fbLoginButton.readPermissions = @[@"public_profile", @"email", @"user_friends", @"user_birthday"];
        _fbLoginButton.delegate = self;
    }
    
    return self;
}

-(void)viewWillAppear:(BOOL)animated{
    [[AppViewController sharedInstance] hideMenuButton];
}

-(void)loginViewFetchedUserInfo:(FBLoginView *)loginView user:(id<FBGraphUser>)user{
    _fbLoginButton.delegate = nil;
    [User sharedInstance].user = user;
    _user = user;
    _fbAccessToken = [FBSession activeSession].accessTokenData.accessToken;
    User *userObj = [User sharedInstance];
    userObj.token = _fbAccessToken;
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        [userObj loginUser:@{
                                    @"fbid" : [_user objectForKey:@"id"],
                                    @"facebook_token" : _fbAccessToken
                                    }
                       completion:^(NSDictionary *loginUserRespData) {
                           
                           if(((NSNumber*)loginUserRespData[@"status"]).integerValue == 200){
                               [userObj createUser:@{
                                                          @"first_name" : _user[@"first_name"],
                                                          @"last_name" :  _user[@"last_name"],
                                                          @"birthday" : _user.birthday,
                                                          @"fbid" : _user.objectID,
                                                          @"facebook_token" : _fbAccessToken,
                                                          @"city" : @"berkeley",
                                                          @"app_version" : [Installation appVersion],
                                                          @"installation_id" : [Installation udid],
                                                          @"device" : @"ios"
                                                          }
                                        completion:^(NSDictionary *createUserRespData) {
                                            NSLog(@"%@", createUserRespData);
                                            User *user = [User sharedInstance];
                                            user.token = createUserRespData[@"data"][@"token_id"];
                                            
                                            [user loadNewFoodWithCompletion:^(NSDictionary *foodData) {
                                                for (NSDictionary *foodItemData in foodData) {
                                                    CBDish *foodItem = [[CBDish alloc] initWithData:foodItemData];
                                                    [foodItem loadImage];
                                                    
                                                    [user.loadedFoods addObject:foodItem];
                                                }
                                                
                                                
                                                [[AppViewController sharedInstance] loadViewController:[CBDishDiscoveyViewController class]];
                                            }];
                                             }];
                           } else{
                               NSLog(@"%@", loginUserRespData);
                               User *user = [User sharedInstance];
                               user.token = loginUserRespData[@"data"][@"token_id"];
                               
                               [user loadNewFoodWithCompletion:^(NSDictionary *foodData) {
                                   for (NSDictionary *foodItemData in foodData) {
                                       @autoreleasepool {
                                           CBDish *foodItem = [[CBDish alloc] initWithData:foodItemData];
                                           [foodItem loadImage];
                                           
                                           [user.loadedFoods addObject:foodItem];
                                       }
                                   }
                                   
                                   [[AppViewController sharedInstance] loadViewController:[CBDishDiscoveyViewController class]];
                               }];
                           }
                       }];
    });
}

-(void)loginViewShowingLoggedInUser:(FBLoginView *)loginView{
    
}

-(void)loginView:(FBLoginView *)loginView handleError:(NSError *)error{
    
}

@end
