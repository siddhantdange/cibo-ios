//
//  MenuCell.m
//  Cibo
//
//  Created by Siddhant Dange on 11/25/14.
//  Copyright (c) 2014 Siddhant Dange. All rights reserved.
//

#import "MenuCell.h"

@implementation MenuCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
