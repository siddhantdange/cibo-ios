//
//  CBDishTableViewCell.h
//  Cibo
//
//  Created by Siddhant Dange on 6/20/15.
//  Copyright (c) 2015 Siddhant Dange. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "HorizontalTableViewCell.h"

@interface CBDishTableViewCell : HorizontalTableViewCell

extern NSString *CBDishTableViewCellIdentifier;

@property (nonatomic, strong) UIImage *dishImage;

@end
