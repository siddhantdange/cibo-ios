//
//  MenuCell.h
//  Cibo
//
//  Created by Siddhant Dange on 11/25/14.
//  Copyright (c) 2014 Siddhant Dange. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *menuTitleImageView;
@property (weak, nonatomic) IBOutlet UILabel *menuTitleLabel;

@end
