//
//  HorizontalTableView.m
//  SidUI
//
//  Created by Siddhant Dange on 7/5/14.
//  Copyright (c) 2014 Siddhant Dange. All rights reserved.
//

#import "HorizontalTableView.h"
#import "HorizontalTableViewCell.h"

@interface HorizontalTableView ()

@property (nonatomic, strong) UIView *noItemsView;

@end

@implementation HorizontalTableView

-(id)initWithCellNibNames:(NSArray*)cellClassNameArr andFrame:(CGRect)rect{
    HorizontalTableViewCell *cell = [[[NSBundle mainBundle] loadNibNamed:cellClassNameArr[0] owner:self options:nil] objectAtIndex:0];
    
    self = [self initWithFrame:CGRectMake(3.0f, 3.0f, rect.size.width, cell.frame.size.height)];
    if(self){
        
        CGAffineTransform transform = CGAffineTransformMakeRotation(-M_PI_2);
        self.transform = transform;
        
        // Repositions and resizes the view.
        CGRect contentRect = CGRectMake(0.0f, rect.origin.y, rect.size.width, cell.frame.size.width);
        self.frame = contentRect;
        self.contentOffset = CGPointMake(0.0f, 0.0f);
        
        //registers all types of cells
        for (NSString *cellClass in cellClassNameArr) {
            [self registerNib:[UINib nibWithNibName:cellClass bundle:nil] forCellReuseIdentifier:cellClass];
        }
        
        //init vars
        [self setRowHeight:cell.frame.size.height];
        self.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end

#pragma -mark HorizontalTableViewCell

