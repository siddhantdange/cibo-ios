//
//  SlideMenuScreen.h
//  Pal
//
//  Created by Siddhant Dange on 7/22/14.
//  Copyright (c) 2014 Siddhant Dange. All rights reserved.
//

#import "Screen.h"

@interface SlideMenuScreen : Screen <UITableViewDataSource, UITableViewDelegate>

-(id)initWithVerticalSetting:(BOOL)isVertical;
-(void)show;
-(void)close;
    
@end
