//
//  MenuViewController.m
//  Test1
//
//  Created by Siddhant Dange on 5/10/15.
//  Copyright (c) 2015 Siddhant Dange. All rights reserved.
//

#import "MenuViewController.h"
#import "MenuCell.h"
#import "UIColor+Extras.h"

@interface MenuViewController ()

@property (nonatomic, strong) IBOutlet UITableView *menuTableView;
@property (nonatomic, strong) IBOutlet UIButton *closeButton;
@property (nonatomic, strong) NSDictionary *viewControllersInMenu, *viewControllerTitles;
@property (nonatomic, strong) NSArray *menuVCOrder;

@property (nonatomic, assign) id menuDelegate;
@property (nonatomic, assign) SEL menuItemHit, menuCloseSel;

@end

@implementation MenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _menuTableView.dataSource = self;
    _menuTableView.delegate = self;
    _menuTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    _menuTableView.backgroundColor = [UIColor offWhite];
    [_menuTableView registerNib:[UINib nibWithNibName:@"MenuCell" bundle:nil] forCellReuseIdentifier:@"MenuCell"];
}

-(void)setDelegate:(id)menuDelegate menuItemHitSel:(SEL)menuItemHit menuCloseSel:(SEL)menuCloseSel{
    _menuDelegate = menuDelegate;
    _menuItemHit = menuItemHit;
    _menuCloseSel = menuCloseSel;
}

-(void)setMenuViewControllers:(NSDictionary*)viewControllers forTitles:(NSDictionary*)titles forOrder:(NSArray *)classOrder{
    _viewControllersInMenu = viewControllers;
    _viewControllerTitles = titles;
    _menuVCOrder = classOrder;
}

-(float)menuTableWidth{
    return _menuTableView.frame.size.width;
}
- (IBAction)closeMenuTouched:(id)sender {
    if(_menuDelegate && _menuCloseSel){
        [_menuDelegate performSelector:_menuCloseSel withObject:nil];
    }
}

#pragma -mark UITableViewDataSource Methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    int numRows = (int)_viewControllersInMenu.allKeys.count;
    return numRows;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 105.0f;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    // Identifier for retrieving reusable cells.
    static NSString *cellIdentifier = @"MenuCell";
    
    // Attempt to request the reusable cell.
    MenuCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    // No cell available - create one.
    if(cell == nil) {
        cell = [[MenuCell alloc] init];
    }
    
    NSString *classTitle = NSStringFromClass(_menuVCOrder[indexPath.row]);
    NSString *vcTitle = _viewControllerTitles[classTitle];
    
    [cell.menuTitleLabel setText:vcTitle];
    
    return cell;
}

#pragma -mark UITableViewDelegate Methods

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *classTitle = NSStringFromClass(_menuVCOrder[indexPath.row]);
    if(_menuDelegate && _menuItemHit){
        [_menuDelegate performSelector:_menuItemHit withObject:classTitle];
    }
}

@end
