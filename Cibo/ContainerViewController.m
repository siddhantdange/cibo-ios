//
//  ViewController.m
//  Test1
//
//  Created by Siddhant Dange on 5/10/15.
//  Copyright (c) 2015 Siddhant Dange. All rights reserved.
//

#import "ContainerViewController.h"

@interface ContainerViewController ()

@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (nonatomic, strong) UINavigationController* navigationController;
@property (nonatomic, strong) NSMutableDictionary *viewControllersInMenu;

@end

@implementation ContainerViewController

-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if(self){
        
        //prep navigation controller
        _navigationController = [[UINavigationController alloc] init];
        UIViewController *baseVc = [[UIViewController alloc] init];
        [_navigationController pushViewController:baseVc animated:YES];
        [_navigationController setNavigationBarHidden:YES];
        baseVc.navigationItem.hidesBackButton = YES;
        
        [self.view addSubview:_navigationController.view];
    }
    
    return self;
}

-(void)setViewController:(UIViewController*)viewController{
    [_navigationController pushViewController:viewController animated:NO];
    _navigationController.view.frame = viewController.view.frame;
    viewController.navigationItem.hidesBackButton = YES;
}

-(void)pushViewController:(UIViewController*)vc isAnimated:(BOOL)animated{
    [_navigationController pushViewController:vc animated:animated];
    vc.navigationItem.hidesBackButton = YES;
}

-(void)popViewControllerisAnimated:(BOOL)animated{
    [_navigationController popViewControllerAnimated:animated];
}

@end

