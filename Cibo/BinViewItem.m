//
//  BinItemView.m
//  Test1
//
//  Created by Siddhant Dange on 5/20/15.
//  Copyright (c) 2015 Siddhant Dange. All rights reserved.
//

#import "BinViewItem.h"

@implementation BinViewItem

-(instancetype)init{
    self = [super init];
    if(self){
        self = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil][0];
    }
    
    return self;
}

@end
