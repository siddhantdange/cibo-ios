//
//  PreferencesScreen.m
//  Cibo
//
//  Created by Siddhant Dange on 1/15/15.
//  Copyright (c) 2015 Siddhant Dange. All rights reserved.
//

#import "PreferencesScreen.h"

@interface PreferencesScreen ()
@property (weak, nonatomic) IBOutlet UISlider *radiusSlider;
@property (weak, nonatomic) IBOutlet UILabel *radiusLabel;
@property (weak, nonatomic) IBOutlet UISlider *priceSlider;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

@end

@implementation PreferencesScreen

-(instancetype)init{
    self = [super init];
    if(self){
        [[NSBundle mainBundle] loadNibNamed:@"PreferencesScreen" owner:self options:nil];
    }
    return self;
}
-(void)viewDidLoad{
    [_radiusLabel setText:[NSString stringWithFormat:@"%d", (int)_radiusSlider.value]];
    [_priceLabel setText:[NSString stringWithFormat:@"%d", (int)_priceSlider.value]];
}

- (IBAction)sliderMoved:(id)sender {
    UISlider *slider = (UISlider*)sender;
    
    if(slider == _radiusSlider){
        
    } else if(slider == _priceSlider){
        
    }
}


@end
