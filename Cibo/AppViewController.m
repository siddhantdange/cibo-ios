//
//  AppViewController.m
//  Test1
//
//  Created by Siddhant Dange on 5/10/15.
//  Copyright (c) 2015 Siddhant Dange. All rights reserved.
//

#import "AppViewController.h"
#import "ContainerViewController.h"
#import "MenuViewController.h"
#import "UIColor+Extras.h"

@interface AppViewController ()

@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UIScrollView *outerScrollView;
@property (nonatomic, strong) UIButton *menuOpenButton;
@property (nonatomic, strong) MenuViewController *menuVc;
@property (nonatomic, strong) ContainerViewController *containerVc;
@property (nonatomic, strong) NSMutableDictionary *viewControllersInMenu;

@end

static AppViewController *gInstance;
@implementation AppViewController

-(instancetype)initWithViewControllersInMenu:(NSArray*)vcArr forTitles:(NSDictionary*)vcTitles withInitialViewController:(Class)initVc{
    
    self = [super init];
    
    if(self){
        
        //create self view
        self.view = [[NSBundle mainBundle] loadNibNamed:@"AppViewController" owner:self options:nil][0];
        
        //init menu and container
        _menuVc = [[MenuViewController alloc] initWithNibName:@"MenuViewController" bundle:nil];
        _containerVc = [[ContainerViewController alloc] initWithNibName:@"ContainerViewController" bundle:nil];
        [_outerScrollView addSubview:_menuVc.view];
        [_outerScrollView addSubview:_containerVc.view];
        
        //init every VC
        _viewControllersInMenu = @{}.mutableCopy;
        for (Class vcClass in vcArr) {
            UIViewController *vc = [[vcClass alloc] init];
            [_viewControllersInMenu setObject:vc forKey:NSStringFromClass(vcClass)];
        }
        
        //init initial vc
        UIViewController *innerVc;
        if ([((NSArray*)_viewControllersInMenu.allKeys) containsObject:NSStringFromClass(initVc)]) {
            innerVc = _viewControllersInMenu[NSStringFromClass(initVc)];
        } else{
            innerVc = [[initVc alloc] init];
        }
        
        //push to container vc
        [_containerVc setViewController:innerVc];
        [self.view bringSubviewToFront:_menuVc.view];
        
        //prep menu
        [_menuVc setDelegate:self menuItemHitSel:@selector(menuItemHit:) menuCloseSel:@selector(closeMenu)];
        [_menuVc setMenuViewControllers:_viewControllersInMenu forTitles:vcTitles forOrder:vcArr];
        
        //adjust self view to fit menu and content
        CGSize contentFrame = _outerScrollView.contentSize;
        contentFrame.width = CGRectGetWidth(_containerVc.view.frame) + [_menuVc menuTableWidth];
        _outerScrollView.contentSize = contentFrame;
        CGPoint offset = _outerScrollView.contentOffset;
        offset.x = [_menuVc menuTableWidth];
        _outerScrollView.contentOffset = offset;
        
        CGRect containerRect = _containerVc.view.frame;
        containerRect.origin.x += [_menuVc menuTableWidth];
        _containerVc.view.frame = containerRect;
        
        //add menu open button
        
        _menuOpenButton = [[UIButton alloc] initWithFrame:CGRectMake(15.0f + [_menuVc menuTableWidth], 95.0f, 50.0f, 50.0f)];
        [_menuOpenButton setBackgroundImage:[UIImage imageNamed:@"settings_button"] forState:UIControlStateNormal];
        _menuOpenButton.backgroundColor = [UIColor offWhite];
        [_menuOpenButton.titleLabel setText:@"Menu"];
        [_menuOpenButton addTarget:self action:@selector(menuButtonHit) forControlEvents:UIControlEventTouchUpInside];
        [_outerScrollView addSubview:_menuOpenButton];
        [_outerScrollView bringSubviewToFront:_menuOpenButton];
        
        gInstance = self;
    }
    
    return self;
}

-(BOOL)prefersStatusBarHidden{
    return YES;
}

-(void)showMenuButton{
    [_menuOpenButton setHidden:NO];
}

-(void)hideMenuButton{
    [_menuOpenButton setHidden:YES];
}

-(void)menuButtonHit{
   [self openMenu];
}

-(void)loadViewController:(Class)vcClass{
    UIViewController *viewController = nil;
    if([_viewControllersInMenu.allKeys containsObject:NSStringFromClass(vcClass)]){
        viewController = _viewControllersInMenu[NSStringFromClass(vcClass)];
    } else{
        viewController = [[vcClass alloc] initWithNibName:NSStringFromClass(vcClass) bundle:nil];
    }
    
    [[NSBundle mainBundle] loadNibNamed:NSStringFromClass(vcClass) owner:viewController options:nil];
    viewController.view.hidden = NO;
    [self loadVCInContainer:viewController];
}

#pragma -mark Menu

-(void)openMenu{
    [_outerScrollView insertSubview:_menuVc.view belowSubview:_menuOpenButton];
    [UIView animateWithDuration:0.2f animations:^{
        CGPoint p = _outerScrollView.contentOffset;
        p.x = 0;
        _outerScrollView.contentOffset = p;
    } completion:^(BOOL finished) {
        
    }];
}

-(void)closeMenu{
    [_outerScrollView insertSubview:_containerVc.view belowSubview:_menuOpenButton];
    [UIView animateWithDuration:0.2f animations:^{
        
        CGPoint p = _outerScrollView.contentOffset;
        p.x = [_menuVc menuTableWidth];
        _outerScrollView.contentOffset = p;
    } completion:^(BOOL finished) {
        
    }];

}

-(void)menuItemHit:(NSString*)className{
    [self loadVCInContainer:_viewControllersInMenu[className]];
    [self closeMenu];
}

-(void)loadVCInContainer:(UIViewController*)viewController{
    [_containerVc popViewControllerisAnimated:NO];
    [_containerVc pushViewController:viewController isAnimated:NO];
}

#pragma -mark Singleton

+ (instancetype)sharedInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if(!gInstance){
            gInstance = [[AppViewController alloc] init];
        }
        
        
    });
            
    return gInstance;
}
@end
