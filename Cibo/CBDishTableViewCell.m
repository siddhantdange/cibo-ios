//
//  CBDishTableViewCell.m
//  Cibo
//
//  Created by Siddhant Dange on 6/20/15.
//  Copyright (c) 2015 Siddhant Dange. All rights reserved.
//

#import "CBDishTableViewCell.h"

#import <CoreMotion/CoreMotion.h>

#import "DishInfoView.h"
#import "Installation.h"
#import "CBHud.h"

@interface CBDishTableViewCell ()

@property (weak, nonatomic) IBOutlet UIScrollView *containerView;
@property (weak, nonatomic) IBOutlet UIScrollView *infoScrollView;

@property (nonatomic, strong) CMMotionManager *motionManager;
@property (nonatomic, strong) UIDynamicAnimator *animator;
@property (nonatomic, strong) UIDynamicItemBehavior *itemBehavior;

@property (nonatomic, strong) DishInfoView *dishInfoView;
@property (nonatomic, strong) CBHud *hudView;
@property (nonatomic, strong) UIImageView *dishImageView;

@end

const NSString *CBDishTableViewCellIdentifier = @"CBDishTableViewCell";

@implementation CBDishTableViewCell

-(instancetype)init {
	self = [super init];    [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil];
	if(self) {
		int pageCount = 2;
		_infoScrollView.pagingEnabled = YES;
		_infoScrollView.contentSize = CGSizeMake(_infoScrollView.frame.size.width, _infoScrollView.frame.size.height*pageCount);
		_infoScrollView.contentOffset = CGPointMake(0.0f, (_infoScrollView.contentSize.height)/2.0f);
		//[_infoScrollView setDelegate:self];

		DishInfoView *dishInfoView = [[DishInfoView alloc] init];
		_dishInfoView = dishInfoView;
		[_infoScrollView addSubview:dishInfoView];

		[self showPreview];

		_animator = [[UIDynamicAnimator alloc] initWithReferenceView:_infoScrollView];

		// Gradient to top image
		UIView *perfectPixelContent = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.bounds), 1)];
		perfectPixelContent.backgroundColor = [UIColor colorWithWhite:1 alpha:0.2];
		[self addSubview:perfectPixelContent];


		self.layer.cornerRadius = 5.0f;
		self.dishImageView.layer.cornerRadius = 5.0f;
		//_containerView.clipsToBounds = YES;

		UIPanGestureRecognizer *g = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(dragged:)];
		[_dishInfoView addGestureRecognizer:g];
	}

	return self;
}

-(void)setDishName:(NSString *)dishName {
	[_dishInfoView setDishName:dishName];
}

-(void)setRestaurantName:(NSString *)restaurantName {
	[_dishInfoView.restaurantNameLabel setText:restaurantName];
}

-(void)setRestaurantLat:(float)lat lon:(float)lon {

}

-(void)setPrice:(NSString *)price {
	[_dishInfoView.priceLabel setText:price];
}

-(void)setScrollEffects {
	//init motion
	self.motionManager = [[CMMotionManager alloc] init];
	self.motionManager.accelerometerUpdateInterval = 1;

	//Gyroscope
	if([self.motionManager isGyroAvailable]) {
		/* Start the gyroscope if it is not active already */
		if([self.motionManager isGyroActive] == NO) {
			/* Update us 2 times a second */
			[self.motionManager setGyroUpdateInterval:1.0f / 100.0f];

			/* Add on a handler block object */

			/* Receive the gyroscope data on this block */
			[self.motionManager startGyroUpdatesToQueue:[NSOperationQueue mainQueue]
			 withHandler:^(CMGyroData *gyroData, NSError *error) {
			         CGPoint newOffset = CGPointMake(self.containerView.contentOffset.x + gyroData.rotationRate
			                                         .y, 0.0f);
			         if(newOffset.x <= self.dishImage.size.width - self.frame.size.width && newOffset.x >= 0) {
			                 self.containerView.contentOffset = newOffset;
				 }
			 }];
		}
	} else {
		NSLog(@"Gyroscope not Available!");
	}

}

-(void)showPreview {
	_hudView = [[CBHud alloc] init];
	_hudView.center = self.center;

	[self addSubview:_hudView];
	[_hudView startAnimating];
}

-(void)hidePreview {
	[_hudView stopAnimating];
	[_hudView removeFromSuperview];
	_hudView = nil;
}

-(void)setDishImage:(UIImage *)dishImage {
	self.dishImageView.image = nil;
	_dishImage = [self resizeImageToFit:dishImage];
	self.dishImageView.image = _dishImage;
	[self.containerView setContentOffset:
	 CGPointMake((_dishImage.size.width - self.frame.size.width)/2.0f, 0.0f)
	];
	[self.containerView setContentSize:self.dishImageView.bounds.size];
	[self setScrollEffects];
}

-(UIImage*)resizeImageToFit:(UIImage*)image {
	float imageFactor = image.size.height/[Installation screenHeight];
	return [UIImage imageWithCGImage:image.CGImage scale:imageFactor orientation:UIImageOrientationUp];
}

@end
