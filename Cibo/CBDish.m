//
//  FoodItem.m
//  Cibo
//
//  Created by Siddhant Dange on 5/28/15.
//  Copyright (c) 2015 Siddhant Dange. All rights reserved.
//

@import ImageIO;
#import "CBDish.h"

@interface CBDish ()

@property (nonatomic, assign) BOOL hasLoadedImage;

@end

@implementation CBDish

-(instancetype)initWithData:(NSDictionary*)data{
    self = [super init];
    if(self){
        self.imageUrl = [NSURL URLWithString:data[@"image_url"]];
        self.dishName = data[@"name"];
        self.restaurantName = data[@"restaurant_name"];
        self.dishId = data[@"id"];
        self.price = @(((NSString*)data[@"price"]).floatValue);
        self.hasLoadedImage = NO;
    }
    return self;
}

-(void)loadImage{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:self.imageUrl]];
        
        if(image){
            [self writeImage:image];
            self.hasLoadedImage = YES;
            dispatch_async(dispatch_get_main_queue(), ^{
                self.image = image;
                if(self.imageLoadCompletion){
                    self.imageLoadCompletion(self.image);
                }
            });
        }
        
    });
}

-(UIImage*)getImage{
    if (!self.image) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            self.image = [self readImage];
            if(self.imageLoadCompletion){
                self.imageLoadCompletion(self.image);
            }
        });
    }
    
    return self.image;
}

-(void)writeImage:(UIImage*)image{
    
//    CGImageSourceRef src = CGImageSourceCreateWithData((CFDataRef)image, NULL);
//    CFDictionaryRef options = (__bridge CFDictionaryRef)[[NSDictionary alloc] initWithObjectsAndKeys:(id)kCFBooleanTrue, (id)kCGImageSourceCreateThumbnailWithTransform, (id)kCFBooleanTrue, (id)kCGImageSourceCreateThumbnailFromImageIfAbsent, (id)[NSNumber numberWithDouble:200.0], (id)kCGImageSourceThumbnailMaxPixelSize, nil];
//    CGImageRef thumbnail = CGImageSourceCreateThumbnailAtIndex(src, 0, options);
//    
//    UIImage *img = [[UIImage alloc] initWithCGImage:thumbnail];
//    // Cache
//    NSString *fileName = _dishId;
//    NSString *path = [NSTemporaryDirectory() stringByAppendingPathComponent:@"thumbnail"];
//    
//    path = [path stringByAppendingPathComponent:fileName];
//    if ([UIImagePNGRepresentation(image) writeToFile:path atomically:YES]) {
//        // Success
//    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // Create path.
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:self.dishId];
        
        // Save image.
        [UIImagePNGRepresentation(image) writeToFile:filePath atomically:YES];
    });
}

-(UIImage*)readImage{
    // Create path.
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:self.dishId];
    
    return [UIImage imageWithContentsOfFile:filePath];
}

-(BOOL)hasDownloadedImage{
    return [self getImage] != nil;
}

-(BOOL)hasCachedImage{
    return self.image != nil;
}

-(NSString *)description{
    NSMutableString *descriptionStr = @"".mutableCopy;
    [descriptionStr appendFormat:@"Name: %@\n", self.dishName];
    [descriptionStr appendFormat:@"Id: %@\n", self.dishId];
    [descriptionStr appendFormat:@"Price: %@\n", self.price];
    [descriptionStr appendFormat:@"Restaurant: %@\n", self.restaurantName];
    [descriptionStr appendFormat:@"ImgCached: %d\n", [self hasCachedImage]];
    [descriptionStr appendFormat:@"ImgDownloaded: %d\n", [self hasDownloadedImage]];
    
    return descriptionStr.copy;
}

@end
