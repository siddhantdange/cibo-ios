//
//  CBHud.m
//  Cibo
//
//  Created by Siddhant Dange on 5/28/15.
//  Copyright (c) 2015 Siddhant Dange. All rights reserved.
//

#import "CBHud.h"

@interface CBHud ()

@property (nonatomic, strong) UIImage *hudImage;
@property (weak, nonatomic) IBOutlet UIImageView *hudImageView;

@end

@implementation CBHud

-(instancetype)init{
    self = [super init];
    if(self){
        self = [[NSBundle mainBundle] loadNibNamed:@"CBHud" owner:self options:nil][0];
        
        self.hudImage = [UIImage imageNamed:@"Icon-76"];
        [self.hudImageView setImage:self.hudImage];
    }
    return self;
}

-(void)startAnimating{
    
    CABasicAnimation *rotation;
    rotation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    rotation.fromValue = [NSNumber numberWithFloat:0];
    rotation.toValue = [NSNumber numberWithFloat:(2 * M_PI)];
    rotation.duration = 1.5f; // Speed
    rotation.repeatCount = HUGE_VALF; // Repeat forever. Can be a finite number.
    [self.layer removeAllAnimations];
    [self.layer addAnimation:rotation forKey:@"Spin"];
}

-(void)stopAnimating{
    [self.layer removeAllAnimations];
}

@end
