//
//  Installation.h
//  Cibo
//
//  Created by Siddhant Dange on 10/22/14.
//  Copyright (c) 2014 Siddhant Dange. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Installation : NSObject

+(NSString*)udid;
+(NSString*)appVersion;
+(float)screenHeight;
+(float)screenWidth;
@end
