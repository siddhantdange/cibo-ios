//
//  UIView+Extras.m
//  SidUI
//
//  Created by Siddhant Dange on 7/14/14.
//  Copyright (c) 2014 Siddhant Dange. All rights reserved.
//

#import "UIView+Extras.h"

@implementation UIView (Extras)

-(float)height{
    return self.frame.size.height;
}

-(void)setHeight:(float)height{
    CGRect frame = self.frame;
    frame.size.height = height;
    self.frame = frame;
}

-(float)width{
    return self.frame.size.width;
}

-(void)setWidth:(float)width{
    CGRect frame = self.frame;
    frame.size.width = width;
    self.frame = frame;
}

-(float)x{
    return self.frame.origin.x;
}

-(void)setX:(float)x{
    CGRect frame = self.frame;
    frame.origin.x = x;
    self.frame = frame;
}

-(float)y{
    return self.frame.origin.y;
}

-(void)setY:(float)y{
    CGRect frame = self.frame;
    frame.origin.y = y;
    self.frame = frame;
}

@end
