//
//  ViewController.h
//  Cibo
//
//  Created by Siddhant Dange on 10/20/14.
//  Copyright (c) 2014 Siddhant Dange. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import "Screen.h"

@interface CBLoginViewController : UIViewController <FBLoginViewDelegate>


@end

