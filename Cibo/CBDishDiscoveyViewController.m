//
//  FoodViewScreen.m
//  Cibo
//
//  Created by Siddhant Dange on 10/20/14.
//  Copyright (c) 2014 Siddhant Dange. All rights reserved.
//

#import "CBDishDiscoveyViewController.h"
#import "AppDelegate.h"
#import "AppViewController.h"
#import "User.h"

#import "HorizontalTableView.h"
#import "CBDishTableViewCell.h"

#import "CBDish.h"

@interface CBDishDiscoveyViewController ()

@property (nonatomic, strong) NSMutableArray *likedDishes, *fullImageData;
@property (nonatomic, weak) NSMutableArray *foodItems;

@property (nonatomic, strong) HorizontalTableView *dishTableView;

@end

@implementation CBDishDiscoveyViewController

-(instancetype)init{
    self = [super init];
    if(self){
        self.view = [[NSBundle mainBundle] loadNibNamed:@"CBDishDiscoveyViewController" owner:self options:nil][0];
        
    }
    
    return self;
}

-(void)viewWillAppear:(BOOL)animated{
    [self.navigationController.navigationBar setHidden:YES];
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    
    [[AppViewController sharedInstance] showMenuButton];
    
    User *user = [User sharedInstance];
    self.foodItems = user.loadedFoods;
    
    if(!self.dishTableView.superview){
        [self.view addSubview:self.dishTableView];
    }
    
}

-(HorizontalTableView *)dishTableView{
    if (!_dishTableView) {
        _dishTableView = [[HorizontalTableView alloc] initWithCellNibNames:@[[CBDishTableViewCell class]] andFrame:self.view.frame];
    }
    
    return _dishTableView;
}

#pragma -mark UITableViewDataSource Methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    int numRows = 10;
    return numRows;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    // Identifier for retrieving reusable cells.
    NSString *cellIdentifier = NSStringFromClass([CBDishTableViewCell class]);
    
    // Attempt to request the reusable cell.
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    // No cell available - create one.
    if(cell == nil) {
        cell = [[UITableViewCell alloc] init];
    }
    
    
    return cell;
}

@end
