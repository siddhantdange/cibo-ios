//
//  ViewController.h
//  Test1
//
//  Created by Siddhant Dange on 5/10/15.
//  Copyright (c) 2015 Siddhant Dange. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContainerViewController : UIViewController

-(void)setViewController:(UIViewController*)viewController;

-(void)pushViewController:(UIViewController*)vc isAnimated:(BOOL)animated;
-(void)popViewControllerisAnimated:(BOOL)animated;

@end

