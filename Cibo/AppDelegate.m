//
//  AppDelegate.m
//  Cibo
//
//  Created by Siddhant Dange on 10/20/14.
//  Copyright (c) 2014 Siddhant Dange. All rights reserved.
//

#import "AppDelegate.h"
#import "AppViewController.h"


#import "ScreenNavigation.h"
#import "PreferencesScreen.h"
#import "CBDishDiscoveyViewController.h"
#import "CBLoginViewController.h"
#import "CBLikedDishViewController.h"

#import <FacebookSDK/FacebookSDK.h>

@interface AppDelegate ()

@property (nonatomic, strong) UIWindow *menuWindow;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [FBLoginView class];
    
    //make main navigation
    
    // Override point for customization after application launch.
    AppViewController *app = [[AppViewController alloc] initWithViewControllersInMenu:@[
                                                                                        [CBDishDiscoveyViewController class],
                                                                                        [PreferencesScreen class], [CBLikedDishViewController class]
                                                                                        ]
                                                                            forTitles: @{
                                                                                         NSStringFromClass([CBDishDiscoveyViewController class]) : @"Dishes",
                                                                                         NSStringFromClass([PreferencesScreen class]) : @"Preferences",
                                                                                         NSStringFromClass([CBLikedDishViewController class]) : @"Liked Dishes"
                                                                                         }
                                                            withInitialViewController:[CBLoginViewController class]
                              ];
    
    //make main navigation
    _window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    _screenNavigation = [[ScreenNavigation alloc] init];
    [_screenNavigation setNavigationBarHidden:YES];
    app.navigationItem.hidesBackButton = YES;
    [_screenNavigation pushViewController:app animated:YES];
    _window.rootViewController = _screenNavigation;
    [_window addSubview:_screenNavigation.view];
    [_window makeKeyAndVisible];
    
    //start with LoginScreen
    
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    
    // Call FBAppCall's handleOpenURL:sourceApplication to handle Facebook app responses
    BOOL wasHandled = [FBAppCall handleOpenURL:url sourceApplication:sourceApplication];
    
    // You can add your app-specific url handling code here if needed
    
    return wasHandled;
}

@end
