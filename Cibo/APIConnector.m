//
//  APIConnector.m
//  Cibo
//
//  Created by Siddhant Dange on 10/22/14.
//  Copyright (c) 2014 Siddhant Dange. All rights reserved.
//

#import "APIConnector.h"
#import "User.h"

#define BASE_URL @"http://20b32699.ngrok.com"
//#define BASE_URL @"http://www.cibo.me"

@implementation APIConnector

#pragma -mark Behavior

-(void)sendBehavior:(NSString*)behaviorType forFoodId:(NSString*)foodId completion:(void(^)(NSDictionary*))completion{
    if([behaviorType isEqualToString:@"LIKE"]){
        [self requestMethod:@"POST"
                           withPath:@"/user/behavior/dish/like"
                            andData:@{
                                      @"dish_id" : foodId,
                                      @"token" : [User sharedInstance].token
                                      } completion:completion];
    } else if([behaviorType isEqualToString:@"DISLIKE"]){
        [self requestMethod:@"POST"
                           withPath:@"/user/behavior/dish/dislike"
                            andData:@{
                                      @"dish_id" : foodId,
                                      @"token" : [User sharedInstance].token
                                      } completion:completion];
    }
}

#pragma -mark Food

-(void)loadNewFoodWithCompletion:(void(^)(NSDictionary*))completion{
    [self requestMethod:@"POST"
                       withPath:@"/dish/get_dishes"
                        andData:@{
                                  @"token" : [User sharedInstance].token,
                                  @"base_num" : @([User sharedInstance].food_base_num)
                        }
                     completion:completion];
}

#pragma -mark User

-(void)createUser:(NSDictionary*)data completion:(void(^)(NSDictionary*))completion{
    [self requestMethod:@"POST" withPath:@"/user/create" andData:data completion:completion];
}

-(void)loginUser:(NSDictionary*)data completion:(void(^)(NSDictionary*))completion{
    [self requestMethod:@"POST" withPath:@"/user/login" andData:data completion:completion];
}

#pragma -mark Request

-(void)requestMethod:(NSString*)method withPath:(NSString*)path andData:(NSDictionary*)data completion:(void(^)(NSDictionary*))completion{
    
    NSURL *url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@%@", BASE_URL, path]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    
    request.HTTPMethod = method;
    
    if(data){
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:data
                                                           options:NSJSONWritingPrettyPrinted
                                                             error:&error];
        [request setHTTPBody:jsonData];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    }
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        //JSON array or object
        NSError *jsonError = nil;
        NSDictionary *resultDict = @{};
        if(data){
            resultDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonError];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            if(completion){
                completion(resultDict);
            }
        });
    }];
}

-(NSString*)stringFromDict:(NSDictionary*)dict{
    NSMutableString *data = @"".mutableCopy;
    for(NSObject *key in dict){
        [data appendFormat:@"&%@=%@",key,dict[key]];
    }
    
    return data;
}

@end
