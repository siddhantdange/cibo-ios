//
//  User.m
//  Cibo
//
//  Created by Siddhant Dange on 10/24/14.
//  Copyright (c) 2014 Siddhant Dange. All rights reserved.
//

#import "User.h"
#import "APIConnector.h"

static User *gInstance;

@interface User ()
@property (nonatomic, strong) APIConnector *apiConnector;

@end
@implementation User


-(void)likeDishForFoodId:(NSString*)foodId completion:(void(^)(NSDictionary*))completion{
    [_apiConnector sendBehavior:@"LIKE" forFoodId:foodId completion:completion];
}

-(void)dislikeDishForFoodId:(NSString*)foodId completion:(void(^)(NSDictionary*))completion{
    [_apiConnector sendBehavior:@"DISLIKE" forFoodId:foodId completion:completion];
}

-(void)loadNewFoodWithCompletion:(void(^)(NSDictionary*))completion{
    [_apiConnector loadNewFoodWithCompletion:completion];
}

-(void)createUser:(NSDictionary*)data completion:(void(^)(NSDictionary*))completion{
    [_apiConnector createUser:data completion:completion];
}

-(void)loginUser:(NSDictionary*)data completion:(void(^)(NSDictionary*))completion{
    [_apiConnector loginUser:data completion:completion];
}



+ (instancetype)sharedInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        gInstance = [[User alloc] init];
        
        gInstance.apiConnector = [[APIConnector alloc] init];
        gInstance.loadedFoods = [NSMutableArray new];
        gInstance.likedDishes = [NSMutableArray new];
        gInstance.food_base_num = 0;
        gInstance.foodIndex = 0;
        
        
    });
            
    return gInstance;
}
@end
