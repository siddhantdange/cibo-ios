//
//  UIColor+Extras.m
//  Cibo
//
//  Created by Siddhant Dange on 11/25/14.
//  Copyright (c) 2014 Siddhant Dange. All rights reserved.
//

#import "UIColor+Extras.h"

@implementation UIColor(Extras)


+(UIColor*)offWhite{
    return [UIColor colorWithRed:0xF9 green:0xF9 blue:0xF9 alpha:0xFF];
}

+(UIColor*)ciboOrange{
    return [UIColor colorWithRed:0xEE green:0x42 blue:0x4D alpha:0xFF];
}

@end
