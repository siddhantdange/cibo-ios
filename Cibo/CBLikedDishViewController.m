//
//  CBLikedDishViewController.m
//  Cibo
//
//  Created by Siddhant Dange on 5/28/15.
//  Copyright (c) 2015 Siddhant Dange. All rights reserved.
//

#import "CBLikedDishViewController.h"
#import "CBLikedDishCollectionViewCell.h"

#import "User.h"
#import "Installation.h"
#import "CBDish.h"

@interface CBLikedDishViewController ()
@property (weak, nonatomic) IBOutlet UICollectionView *likedDishCollectionView;
@property (nonatomic, assign) float widthLeft;
@property (nonatomic, weak) NSMutableArray *likedDishes;

@end

@implementation CBLikedDishViewController

-(instancetype)init{
    self = [super init];
    if(self){
        self.view = [[NSBundle mainBundle] loadNibNamed:@"CBLikedDishViewController" owner:self options:nil][0];
        
        _likedDishCollectionView.dataSource = self;
        _likedDishCollectionView.delegate = self;
        [_likedDishCollectionView registerNib:[UINib nibWithNibName:@"CBLikedDishCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"CBLikedDishCollectionViewCell"];
        _likedDishes = [User sharedInstance].likedDishes;
        for(CBDish *dish in _likedDishes){
            [dish getImage];
        }
    }
    return self;
}

-(void)viewDidAppear:(BOOL)animated{
    [_likedDishCollectionView reloadData];
}

#pragma -mark UICollectionViewDataSource

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return (int)_likedDishes.count;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    CBLikedDishCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CBLikedDishCollectionViewCell" forIndexPath:indexPath];
    cell.hidden = NO;
    CBDish *dish = _likedDishes[indexPath.row];
    
    
    if([dish hasCachedImage]){
        UIImage *img = [self resizeImageToFit:dish.image];
        UIImageView *imageView = [[UIImageView alloc] initWithImage:img];
        
        [cell.containerView addSubview:imageView];
        [cell.containerView setContentOffset:CGPointMake((img.size.width - cell.frame.size.width)/2.0f, (img.size.height - cell.frame.size.height)/2.0f)];
        [cell.containerView setContentSize:imageView.bounds.size];
        imageView = nil;
    } else{
        [dish getImage];
        cell.fadeView.alpha = 1.0f;
        dish.imageLoadCompletion = ^(UIImage* image){
            dispatch_async(dispatch_get_main_queue(), ^{
                UIImage *img = [self resizeImageToFit:image];
                UIImageView *imageView = [[UIImageView alloc] initWithImage:img];
                [cell.containerView addSubview:imageView];
                [cell.containerView setContentOffset:CGPointMake((img.size.width - cell.frame.size.width)/2.0f, (img.size.height - cell.frame.size.height)/2.0f)];
                [cell.containerView setContentSize:imageView.bounds.size];
                imageView = nil;
                
                [UIView animateWithDuration:0.1f animations:^{
                    cell.fadeView.alpha = 0.3f;
                }];
            });
        };
    }
    
    [cell.dishNameLabel setText:dish.dishName];
    
    return cell;
}

-(UIImage*)resizeImageToFit:(UIImage*)image{
    float imageFactor = image.size.width/340.0f;//[Installation screenWidth];
    return [UIImage imageWithCGImage:image.CGImage scale:imageFactor orientation:UIImageOrientationUp];
}

#pragma mark – UICollectionViewDelegateFlowLayout


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if(_widthLeft == 0){
        _widthLeft = 350.0f;
    }
    
    //190 - 140
    //340
    float width = 340.0f;
    return CGSizeMake(width, 100);
    
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CBLikedDishCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CBLikedDishCollectionViewCell" forIndexPath:indexPath];
    
    CBDish *dish = _likedDishes[indexPath.row];
    
    [UIView animateWithDuration:0.1f animations:^{
        cell.fadeView.alpha = 0.0f;
        cell.dishNameLabel.alpha = 0.0f;
    }];
}

// 3
//- (UIEdgeInsets)collectionView:
//(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
//    return UIEdgeInsetsMake(10, 10, 10, 10);
//}

@end
