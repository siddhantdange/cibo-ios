//
//  NSString+Extras.m
//  SidUI
//
//  Created by Siddhant Dange on 7/14/14.
//  Copyright (c) 2014 Siddhant Dange. All rights reserved.
//

#import "NSString+Extras.h"

@implementation NSString (Extras)

-(NSString*)trim{
    return nil;
}

-(NSString*)substringFrom:(int)from to:(int)to{
    return [[self substringFromIndex:from] substringToIndex:(to - from)];
}

@end
