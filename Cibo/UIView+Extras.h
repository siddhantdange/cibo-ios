//
//  UIView+Extras.h
//  SidUI
//
//  Created by Siddhant Dange on 7/14/14.
//  Copyright (c) 2014 Siddhant Dange. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Extras)

@end
