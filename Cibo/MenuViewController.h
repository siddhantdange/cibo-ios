//
//  MenuViewController.h
//  Test1
//
//  Created by Siddhant Dange on 5/10/15.
//  Copyright (c) 2015 Siddhant Dange. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

-(float)menuTableWidth;
-(void)setDelegate:(id)menuDelegate menuItemHitSel:(SEL)menuItemHit menuCloseSel:(SEL)menuCloseSel;
-(void)setMenuViewControllers:(NSDictionary*)viewControllers forTitles:(NSDictionary*)titles forOrder:(NSArray*)classOrder;

@end
