//
//  FoodItem.h
//  Cibo
//
//  Created by Siddhant Dange on 5/28/15.
//  Copyright (c) 2015 Siddhant Dange. All rights reserved.
//

@import UIKit;
#import <Foundation/Foundation.h>

@interface CBDish : NSObject

@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) NSURL *imageUrl;
@property (nonatomic, strong) NSString *dishName, *restaurantName, *dishId;
@property (nonatomic, strong) NSNumber *price;
@property (nonatomic, copy) void (^imageLoadCompletion)(UIImage* __weak);

-(instancetype)initWithData:(NSDictionary*)data;
-(UIImage*)getImage;
-(void)loadImage;
-(BOOL)hasDownloadedImage;
-(BOOL)hasCachedImage;

@end
