//
//  BinItemView.h
//  Cibo
//
//  Created by Siddhant Dange on 10/22/14.
//  Copyright (c) 2014 Siddhant Dange. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BinViewItem.h"

@interface DishBinViewItem : BinViewItem <UIScrollViewDelegate>


-(void)setImage:(UIImage*)image;
-(void)setDishName:(NSString*)dishName;
-(void)setRestaurantName:(NSString*)restaurantName;
-(void)setRestaurantLat:(float)lat lon:(float)lon;
-(void)setPrice:(NSString*)price;

-(void)showPreview;
-(void)hidePreview;

@end
