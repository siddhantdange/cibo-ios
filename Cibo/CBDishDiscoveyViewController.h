//
//  FoodViewScreen.h
//  Cibo
//
//  Created by Siddhant Dange on 10/20/14.
//  Copyright (c) 2014 Siddhant Dange. All rights reserved.
//

#ifndef Cibo_FoodViewScreen_h
#define Cibo_FoodViewScreen_h

@import UIKit;

@interface CBDishDiscoveyViewController : UIViewController <UITableViewDataSource>

@end


#endif
