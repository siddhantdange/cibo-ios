//
//  User.h
//  Cibo
//
//  Created by Siddhant Dange on 10/24/14.
//  Copyright (c) 2014 Siddhant Dange. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FacebookSDK/FacebookSDK.h>

@interface User : NSObject

@property (nonatomic, strong) id<FBGraphUser> user;
@property (nonatomic, strong) NSString *token;
@property (nonatomic, assign) int food_base_num;
@property (nonatomic, strong) NSMutableArray *loadedFoods, *likedDishes, *dislikedDishIds;
@property (nonatomic, assign) int foodIndex;

-(void)likeDishForFoodId:(NSString*)foodId completion:(void(^)(NSDictionary*))completion;
-(void)dislikeDishForFoodId:(NSString*)foodId completion:(void(^)(NSDictionary*))completion;
-(void)createUser:(NSDictionary*)data completion:(void(^)(NSDictionary*))completion;
-(void)loginUser:(NSDictionary*)data completion:(void(^)(NSDictionary*))completion;
-(void)loadNewFoodWithCompletion:(void(^)(NSDictionary*))completion;

+(instancetype)sharedInstance;

@end
