//
//  CBLikedDishViewController.h
//  Cibo
//
//  Created by Siddhant Dange on 5/28/15.
//  Copyright (c) 2015 Siddhant Dange. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CBLikedDishViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@end
