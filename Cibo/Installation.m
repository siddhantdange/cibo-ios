//
//  Installation.m
//  Cibo
//
//  Created by Siddhant Dange on 10/22/14.
//  Copyright (c) 2014 Siddhant Dange. All rights reserved.
//

#import "Installation.h"
#import "UIColor+Extras.h"


@implementation Installation

//colors

+(NSString*)udid{
    return [[[UIDevice currentDevice] identifierForVendor] UUIDString];
}

+(NSString*)appVersion{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
}

+(float)screenHeight{
    return [UIScreen mainScreen].bounds.size.height;
}

+(float)screenWidth{
    return [UIScreen mainScreen].bounds.size.width;
}

@end
